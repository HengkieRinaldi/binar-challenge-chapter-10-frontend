import Head from "next/head"
import GetUser from "../../components/GetUser"

const Profile =() =>{
    return (
        <>
            <Head>
                <title>GAMX | PROFILE</title>
            </Head>
        <GetUser />                
        </>
    )
}

export default Profile