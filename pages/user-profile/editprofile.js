import SideBar from "../../components/SideBar"
import Head from "next/head"
import Link from 'next/link'
import { useRef, useState, useEffect } from "react";
import {
    faCheck,
    faTimes,
    faInfoCircle,
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";




const USER_REGEX = /^[A-z][A-z0-9-_]{2,23}$/;
const PWD_REGEX = /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9]).{8,24}$/;
const EMAIL_REGEX =
/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
const PHONE_REGEX = /^\d{11,13}$/;
const ADDRESS_REGEX = /^[a-zA-Z0-9\s,.'-]{3,}$/;
const BIRTH_REGEX =
/^(0?[1-9]|[12][0-9]|3[01])[\/\-](0?[1-9]|1[012])[\/\-]\d{4}$/;
const REGISTER_URL = "https://traditionalgame.herokuapp.com/api/user";

const UserEdit = () => {
    const userRef = useRef();
    const errRef = useRef();

    const [first_name, setFn] = useState("");
    const [validFn, setValidFn] = useState(false);
    const [fnFocus, setFnFocus] = useState(false);

    const [last_name, setLn] = useState("");
    const [validLn, setValidLn] = useState(false);
    const [lnFocus, setLnFocus] = useState(false);

    const [email, setEmail] = useState("");
    const [validEmail, setValidEmail] = useState(false);
    const [emailFocus, setEmailFocus] = useState(false);

    const [address, setAddress] = useState("");
    const [validAddress, setValidAddress] = useState(false);
    const [addressFocus, setAddressFocus] = useState(false);

    const [phone, setPhone] = useState("");
    const [validPhone, setValidPhone] = useState(false);
    const [phoneFocus, setPhoneFocus] = useState(false);

    const [username, setUser] = useState("");
    const [validName, setValidName] = useState(false);
    const [userFocus, setUserFocus] = useState(false);

    const [password, setPwd] = useState("");
    const [validPwd, setValidPwd] = useState(false);
    const [pwdFocus, setPwdFocus] = useState(false);

    const [matchPwd, setMatchPwd] = useState("");
    const [validMatch, setValidMatch] = useState(false);
    const [matchFocus, setMatchFocus] = useState(false);


    const [errMsg, setErrMsg] = useState("");
    const [success, setSuccess] = useState(false);

    const [avatar, setAvatar] = useState("");

    useEffect(() => {
        userRef.current.focus();
    }, []);

    useEffect(() => {
        setValidName(USER_REGEX.test(username));
        setValidFn(USER_REGEX.test(first_name));
        setValidLn(USER_REGEX.test(last_name));
        setValidEmail(EMAIL_REGEX.test(email));
        setValidPhone(PHONE_REGEX.test(phone));
        setValidAddress(ADDRESS_REGEX.test(address));
    }, [username, first_name, last_name, email, phone, address]);

    useEffect(() => {
        setValidPwd(PWD_REGEX.test(password));
        setValidMatch(password === matchPwd);
    }, [password, matchPwd]);

    useEffect(() => {
        setErrMsg("");
    }, [
        username,
        password,
        matchPwd,
        first_name,
        last_name,
        phone,
        email,
        address,
        avatar
    ]);



    const handleSubmit = async (e) => {
        e.preventDefault();
        let avatar = document.querySelector("#file").files;
        let configFile = new FormData();
        configFile.append('fileData', getFiles[0])
        try {

        const response = await axios.post(
            REGISTER_URL,
            JSON.stringify({
            first_name,
            last_name,
            username,
            email,
            password,
            phone,
            address,
            avatar,
            typeUser: "user"
            }),
            {
            headers: { "Content-Type": "application/json" },
            withCredentials: false,
            },
            {
            data: configFile,
            }
        );
        console.log(response?.data);
        console.log(response?.accessToken);
        console.log(JSON.stringify(response));
        setSuccess(true);
        //clear state and controlled input
        setUser(user.username);
        setPwd(user.password);
        setMatchPwd(user.matchPwd);
        setFn(user.firstname);
        setLn(user.lastname);
        setEmail(user.email);
        setPhone(user.phone);
        setAddress(user.address);
        setAvatar("");
        } catch (err) {
        if (!err?.response) {
            setErrMsg("No Server Response");
        } else if (err.response?.status === 409) {
            setErrMsg("Username Taken");
        } else {
            setErrMsg("Registration Failed");
        }
        errRef.current.focus();
        }
    };



    return (
        <>
        <Head>
            <title>GAMX | PROFILE</title>
        </Head>
        <div className='container' id='user-profile'>
            <div className="row">
                <div className="sidebar-profile col-md-2">
                    <SideBar />
                </div>
                <div className="container col-md-9" id="overview">
                    <div className="col-md-6">

                    <div className="prf">
                            <div className="sm-prf">
                                <img src="/icons8-avatar-64.png" alt=""></img>
                                <h4></h4>
                                <p>Member</p>
                            </div>
                            <hr></hr>
                            <div className="column details-prf">
                                <section className="sections">
                                <p
                                ref={errRef}
                                className={errMsg ? "errmsg" : "offscreen"}
                                aria-live="assertive"
                                >
                                {errMsg}
                                    </p>
                                <form id="formedit" className="forms" onSubmit={handleSubmit}>
                                <label className="labeledit" htmlFor="username">
                                <p>Username:</p>
                                <FontAwesomeIcon
                                    icon={faCheck}
                                    className={validName ? "valid" : "hide"}
                                />
                                <FontAwesomeIcon
                                    icon={faTimes}
                                    className={validName || !username ? "hide" : "invalid"}
                                />
                                </label>
                                <input
                                type="text"
                                id="username"
                                ref={userRef}
                                autoComplete="off"
                                onChange={(e) => setUser(e.target.value)}
                                value={username}
                                required
                                aria-invalid={validName ? "false" : "true"}
                                aria-describedby="uidnote"
                                onFocus={() => setUserFocus(true)}
                                onBlur={() => setUserFocus(false)}
                                />
                                <p
                                id="note"
                                className={
                                    userFocus && username && !validName
                                    ? "instructions"
                                    : "offscreen"
                                }
                                >
                                <FontAwesomeIcon icon={faInfoCircle} />
                                4 to 24 characters.
                                <br />
                                Must begin with a letter.
                                <br />
                                Letters, numbers, underscores, hyphens allowed.
                                </p>
                                
                                <label htmlFor="firstname">
                                <p>Firstname:</p>
                                <FontAwesomeIcon
                                    icon={faCheck}
                                    className={validFn ? "valid" : "hide"}
                                />
                                <FontAwesomeIcon
                                    icon={faTimes}
                                    className={validFn || !first_name ? "hide" : "invalid"}
                                />
                                </label>
                                <input
                                type="text"
                                id="firstname"
                                ref={userRef}
                                autoComplete="off"
                                onChange={(e) => setFn(e.target.value)}
                                value={first_name}
                                required
                                aria-invalid={validFn ? "false" : "true"}
                                aria-describedby="fnnote"
                                onFocus={() => setFnFocus(true)}
                                onBlur={() => setFnFocus(false)}
                                />
                                <p
                                id="note"
                                className={
                                    fnFocus && first_name && !validFn ? "instructions" : "offscreen"
                                }
                                >
                                <FontAwesomeIcon icon={faInfoCircle} />
                                3 to 10 characters.
                                <br />
                                </p>

                                <label htmlFor="lastname">
                                <p>Lastname:</p>
                                <FontAwesomeIcon
                                    icon={faCheck}
                                    className={validLn ? "valid" : "hide"}
                                />
                                <FontAwesomeIcon
                                    icon={faTimes}
                                    className={validLn || !last_name ? "hide" : "invalid"}
                                />
                                </label>
                                <input
                                type="text"
                                id="lastname"
                                ref={userRef}
                                autoComplete="off"
                                onChange={(e) => setLn(e.target.value)}
                                value={last_name}
                                required
                                aria-invalid={last_name ? "false" : "true"}
                                aria-describedby="lnnote"
                                onFocus={() => setLnFocus(true)}
                                onBlur={() => setLnFocus(false)}
                                />
                                <p
                                id="note"
                                className={
                                    lnFocus && last_name && !validLn ? "instructions" : "offscreen"
                                }
                                >
                                <FontAwesomeIcon icon={faInfoCircle} />
                                3 to 10 characters.
                                <br />
                                </p>
                                
                                <label htmlFor="email">
                                <p>Email:</p>
                                <FontAwesomeIcon
                                    icon={faCheck}
                                    className={validEmail ? "valid" : "hide"}
                                />
                                <FontAwesomeIcon
                                    icon={faTimes}
                                    className={validEmail || !email ? "hide" : "invalid"}
                                />
                                </label>
                                <input
                                type="text"
                                id="email"
                                ref={userRef}
                                autoComplete="off"
                                onChange={(e) => setEmail(e.target.value)}
                                value={email}
                                required
                                aria-invalid={validEmail ? "false" : "true"}
                                aria-describedby="emailnote"
                                onFocus={() => setEmailFocus(true)}
                                onBlur={() => setEmailFocus(false)}
                                />
                                <p
                                id="note"
                                className={
                                    emailFocus && email && !validEmail
                                    ? "instructions"
                                    : "offscreen"
                                }
                                >
                                <FontAwesomeIcon icon={faInfoCircle} />
                                Email is invalid
                                <br />
                                </p>
                                

                                <label htmlFor="password">
                                <p>Password:</p>
                                <FontAwesomeIcon
                                    icon={faCheck}
                                    className={validPwd ? "valid" : "hide"}
                                />
                                <FontAwesomeIcon
                                    icon={faTimes}
                                    className={validPwd || !password ? "hide" : "invalid"}
                                />
                                </label>
                                <input
                                type="password"
                                id="password"
                                onChange={(e) => setPwd(e.target.value)}
                                value={password}
                                required
                                aria-invalid={validPwd ? "false" : "true"}
                                aria-describedby="pwdnote"
                                onFocus={() => setPwdFocus(true)}
                                onBlur={() => setPwdFocus(false)}
                                />
                                <p
                                id="note"
                                className={pwdFocus && !validPwd ? "instructions" : "offscreen"}
                                >
                                <FontAwesomeIcon icon={faInfoCircle} />
                                8 to 24 characters.
                                Must include uppercase and lowercase letters, a number and a
                                special character.
                                </p>

                                <label htmlFor="confirm_pwd">
                                <p>Confirm Password:</p>
                                <FontAwesomeIcon
                                    icon={faCheck}
                                    className={validMatch && matchPwd ? "valid" : "hide"}
                                />
                                <FontAwesomeIcon
                                    icon={faTimes}
                                    className={validMatch || !matchPwd ? "hide" : "invalid"}
                                />
                                </label>
                                <input
                                type="password"
                                id="confirm_pwd"
                                onChange={(e) => setMatchPwd(e.target.value)}
                                value={matchPwd}
                                required
                                aria-invalid={validMatch ? "false" : "true"}
                                aria-describedby="confirmnote"
                                onFocus={() => setMatchFocus(true)}
                                onBlur={() => setMatchFocus(false)}
                                />
                                <p
                                id="note"
                                className={
                                    matchFocus && !validMatch ? "instructions" : "offscreen"
                                }
                                >
                                <FontAwesomeIcon icon={faInfoCircle} />
                                Must match the first password input field.
                                </p>

                                <input
                                type="file"
                                accept="image/*"
                                id="file"
                                onChange={setAvatar}
                                
                                />

                                <button
                                className="btn update" disabled={!validName || !validPwd || !validMatch ? true : false}
                                >
                                Update Profile
                                </button>
                                </form>
                                </section> 









                            </div>
                        </div>


                    </div>
                    <div className="col-md-2">
        

                    </div>
                </div>
            </div>
        </div>
    </>
    )
}

export default UserEdit;