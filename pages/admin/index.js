import { useState, useEffect } from "react";
import { useParams } from "react-router-dom";
import axios from "axios";
import { Button, Table } from "react-bootstrap";
import {Link } from "next/link"

const Admin = () => {
    const [users, setUsers] = useState([]);
    const { id } = useParams();
    // const curentport = "http://localhost:7070"
    const curentport = ""

    useEffect(() => {
        getUser();
    }, []);

    const getUser = async () => {
        const response = await axios.get(
            `${curentport}/api/user`,
            {
                headers: { "Content-Type": "application/json" },
                withCredentials: false,
            }
        );
        console.log(response.data);
        setUsers(response.data);
    };

    return (
        <div>
            <Table striped bordered hover>
                <thead>
                    <tr>
                        <th className="justify-content-center text-center">No</th>
                        <th className="justify-content-center text-center">User Name</th>
                        <th className="justify-content-center text-center">Email</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    {users.map((a, b) => (
                        <tr key={b}>
                            <td>{b + 1}</td>
                            <td>{a.username}</td>
                            <td>{a.email}</td>
                            <td>
                                {/* <Link to={`/user-edit/${a._id}`}>
                                    <Button variant="primary">update</Button>
                                </Link> */}
                                <Button onClick={() => deleteUser(a._id)} variant="danger">
                                    {" "}
                                    delete
                                </Button>
                            </td>
                        </tr>
                    ))}
                </tbody>
            </Table>
        </div>
    )
}

export default Admin