import '../styles/globals.css'
import 'bootstrap/dist/css/bootstrap.min.css';
import Head from 'next/head';
import Script from 'next/script';
import '../styles/Main.css'
import '../styles/NavBar.css'
import '../styles/Card.css'
import '../styles/Carousel.css'
import '../styles/Footer.css'
import '../styles/Signup.css'
import "../styles/Login.module.css";
import "../styles/MatchingImages.css"
import '../styles/User-profile.css'
import '../styles/SideBar.css'
import '../styles/Login.css'
import '../styles/Edituser.css'
import '../styles/Games.css'
import '../styles/About.css'

function MyApp({ Component, pageProps }) {
  return (
    <>
      <Head>
        <meta name="viewport" content="width=device-width, initial-scale=1" />
      </Head>
      <Script
      src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js"
      integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW"
      crossorigin="anonymous"/>
      <Component {...pageProps} />
    </>
  )
}

export default MyApp
