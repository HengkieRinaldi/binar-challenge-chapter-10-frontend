import Link from 'next/link'
import { useState } from "react";
// import { setCookie } from 'cookies-next';
import Image from 'next/image';
import app, { auth } from "../api/firebase";
import { signInWithPopup, GoogleAuthProvider } from "firebase/auth";
import jwt from "jwt-decode";



export default function Login() {
    const [username, setUsername] = useState("")
    const [password, setPassword] = useState("")
    const [loadingPopUpDisplay, setLoadingPopUpDisplay] = useState("none")
    
    function submitBtn(){
        setLoadingPopUpDisplay("flex")
        console.log("submit button diklik")
        if(username&&password){
            fetch('https://traditionalgame.herokuapp.com/api/login', {
                method: "POST",
                body: JSON.stringify({username,password}),
                headers: {"Content-type": "application/json; charset=UTF-8"}
            })
            .then(response => response.json()) 
            .then(json => {
                console.log(json)
                console.log(json.result)
                console.log(json.result.data)
                console.log(json.result.data._id)
                console.log(json.result.data.username)
                console.log(json.result.token)
                console.log("ini string json :"+ JSON.stringify([{_id:json.result.data._id,user:json.result.data.username}]))
                let jsonstrting =JSON.stringify({_id:json.result.data._id,user:json.result.data.username});
                setCookie("tgtoken", json.result.token, 30)
                setCookie("user-data", jsonstrting, 30)
                setLoadingPopUpDisplay("none")
                window.location.href = "/";
            })
            .catch(err => {
                console.log(err)
                alert("Login Gagal");
                window.location.href = "/login"
            
            });
            console.log("user & pass kirim")
        }

    }

    function setCookie(name, value, days) {
        var expires = "";
        if (days) {
          var date = new Date();
          date.setTime(date.getTime() + days * 24 * 60 * 60 * 1000);
          expires = "; expires=" + date.toUTCString();
        }
        document.cookie = name + "=" + (value || "") + expires + "; path=/";
    }
    
    const provider = new GoogleAuthProvider();
    // provider.addScope("https://www.googleapis.com/auth/contacts.readonly");

    //ini google sign in button, silahkan dimodifikasi untuk cek loginnnya.
    function googleLogin(){
        setLoadingPopUpDisplay("flex")
        console.log("google button diklik")
        signInWithPopup(auth, provider)
      .then((result) => {
        // This gives you a Google Access Token. You can use it to access the Google API.
        const credential = GoogleAuthProvider.credentialFromResult(result);
        const token = credential.accessToken;
        // The signed-in user info.
        const user = result.user;
        // console.log(
        //   "Google Login Log__________________________________________"
        // );
        // console.log("Ini User :", user);
        // // console.log("Ini User parse :", JSON.stringify(user));
        // console.log("Ini DisplayName :", user.displayName);
        // console.log("Ini email :", user.email);
        // console.log("Ini Result :", result);
        // console.log("Ini Token :", token);
        // console.log(
        //   "Google Login Log__________________________________________"
        // );
        // console.log("Ini UserId :", user.uid);
        let jsonstrting =JSON.stringify({_id:user.uid,user:user.email});
        setCookie("user-data", jsonstrting, 30)
        setCookie("googlelogin", JSON.stringify(user), 3);
        setLoadingPopUpDisplay("none")
        window.location.href = "/";
        
      })
      .catch((error) => {
        // Handle Errors here.
        const errorCode = error.code;
        const errorMessage = error.message;
        // The email of the user's account used.
        const email = error.customData.email;
        // The AuthCredential type that was used.
        const credential = GoogleAuthProvider.credentialFromError(error);
        // ...
      }); 
    }
    function facebookLogin(){

    }


    return (
        <>
           <div className="text-center center-div" id="login">
                <div className="container border py-5">
                    <div className="title pb-5">
                        <h1 className="font-weight-bold">Login</h1>
                        <span>Log in for the existing user</span>
                    </div>
                    <form onSubmit={e=>e.preventDefault()}>
                        <div className="form-group">
                            <input type="text" className="form-control" placeholder="username" name="username" 
                            value={username} onChange={(e) => setUsername(e.target.value)} required />
                        </div>
                        <div className="form group">
                            <input type="password" className="form-control" name="password" placeholder="password" 
                            value={password} onChange={(e) => setPassword(e.target.value)} required />
                        </div>
                        <button type="submit" className="btn btn-success rounded-pill" onClick={submitBtn}>Submit</button>
                    </form>
                    <h6 className='orLoginWith'>Or Login With :</h6>
                    <div className="social-login-group">
                        <button id='googleLoginBtn' onClick={googleLogin}>
                            <Image src="./googleLogo.png" alt='GLogo' width="18px" height="18px"/>Google</button>
                        <button id="facebookLoginBtn" onClick={facebookLogin}>
                            <Image src="./facebookLogo.png" alt='GLogo' width="20px" height="20px"/>Facebook
                        </button>
                    </div>
                    <p>Dont have an account yet?<Link href="/signup">  Sign Up</Link></p>
                    <Link href="/">Back to Homepage</Link>
                </div>
            </div>
            
            <div id='loadingPopUp'style={{display:loadingPopUpDisplay}}>
                <section id="loadingPopUpBox" >
                    <div className="loader"></div>
                    <h2>Log In....</h2>
                </section>
            </div>
        </>
    )
}