import Head from 'next/head'
import React from 'react'
import NavBar from '../components/NavBar'
import Card1 from '../components/Card/Card1'
import { Card2 } from '../components/Card/Card2'
import Card3 from '../components/Card/Card3'
import Carousel1 from '../components/Carousel/Carousel1'
import Footer from '../components/Footer'
import Link from 'next/link'

export default function Home() {


  return (
    <>
      <Head>
        <title>GAMX | HOME</title>
      </Head>
      <NavBar />
      <div className='landing-page'>
        <div className='container' id='section1'>
          <div className='main-text'>
            <div className='row best-game'>
              <h1>BEST <span>GAME</span></h1>
            </div>
            <h1>PLAYING TODAY</h1>
            <p>We entertain billions of users with our games. Our mission to entertain the world goes beyond gaming.</p>
            <div className='button-main'>
              <Link href='/game'>
                <button className='play-btn'>Play Now ↗</button>
              </Link>
              <Link href='/about'>
                <button className='explore-btn'>Explore</button>
              </Link>
            </div>
          </div>
        </div>

        <div className='container' id='section2'>
          <div className='row align-items-center'>
            <div className='text-center'>
              <h1>THE BEST <span>TRAINING</span> FOR</h1>
              <h1>YOUR ABILITIES</h1>
              <p>We measure your skills by taking both written and applied exams in the<br></br>games you love. And we choose the best game for you.</p>
            </div>
            <div className='row card-one'>
              <Card1 />
            </div>
          </div>
        </div>

        <div className='container' id='section3'>
          <div className='row align-items-center'>
            <div className='text-center'>
              <h1><span>Traditional Games</span> as Cultural Heritage</h1>
              <h1>Miss Your Childhood</h1>
              <p>Our mission to entertain the world goes beyond gaming. If you miss your childhood, our teams<br></br>create many innovative and traditional games.</p>
              <div className='video-frame'>
                <video src='/congklak-game.mp4' autoPlay loop muted />
              </div>
            </div>
          </div>
        </div>

        <div className='container' id='section4'>
          <div className='row'>
            <div className='col-md-6'>
              <h1>A YOUNG & <span>IMPACTFUL</span></h1>
              <h1>GAME STUDIO</h1>
              <p>We have a passion for games!. Our teams create innovative and entertaining apps for everyday life.</p>
            </div>
            <div className='card2 col-md-5'>
              <Card2 />
            </div>
          </div>
        </div>

        <div className='container' id='section5'>
          <div className='justify-align-center row'>
            <div className='text-center'>
              <h1>POPULAR <span>GAME</span></h1>
              <p>When you go up to someone who doesnt play video games and ask them to name<br></br>three video games, what do the say? Now thats popularity.</p>
            </div>
            <Carousel1 />
          </div>
        </div>

        <div className='container' id='section6'>
        <div className='justify-align-center row'>
            <div className='text-center'>
              <h1>GREAT GAME <span>MADE BY</span></h1>
              <h1>PASSIONATE PEOPLE</h1>
              <p>Our founding team are veteran individual contributors who have a<br></br>passion for world-building.</p>
            </div>
            <div className='row card-three'>
              <Card3 />
            </div>
          </div>
        </div>

        <div className='container' id='section7'>
          <div className='justify-align-center row'>
              <div className='text-center'>
                <h1>TO GET UPDATE <span>NEW</span> GAMES!</h1>
                <p>Subscribe to get updated on future game releases</p>
                <div className='button-scb'>
                  <div className="flexContainer">
                      <input type="text" className="form-control" name="email" placeholder="Enter Your Email" id="email-subs"/>
                      <button type="submit" className="btn btn-light"><img src='./bytesize_send.png' alt='' /></button>
                  </div>
                </div>
              </div>
          </div>
        </div>
        <Footer />

      </div>
    
    </>
  )
}
