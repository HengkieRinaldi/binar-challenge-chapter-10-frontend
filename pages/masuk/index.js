import { useState } from "react";
import {Form, Button} from "react-bootstrap"
import Axios from "axios"

export default function Masuk() {
    const [username, setUsername] = useState("")
    const [password, setPassword] = useState("")
    
    const handleLogin = async (e) => {
        console.log("aaa")
        try {
            const respon = await Axios.post("http://localhost:7070/api/login",
            {
                username,password
            },
            {
                headers: {"Content-Type": "application/json"},
                withCredentials: true,
            })
            console.log("Ini data",respon.data)
        } catch (err) {
            console.log(err)
        }
    }
    return(
        <div>
            <Form onSubmit={handleLogin}>
                <input type="text" value={username} onChange={(e) => setUsername(e.target.value)}>
                </input>
                <input type="password" value={password} onChange={(e) => setPassword(e.target.value)}>
                </input>
                <Button onClick={handleLogin}>Masuk</Button>
            </Form>
        </div>
    )
}