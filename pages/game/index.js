import React, { useState } from 'react';
import Link from 'next/link';
import Slider from 'react-slick'
import {BsArrowLeft, BsArrowRight,} from 'react-icons/bs';
import Image from 'next/image';

function Games() {
    const imgs = [
        {
                id: 0,
                title: 'Paper Rock Scissors',
                detail: 'Played between two people, in which each player simultaneously forms one of three shapes with an outstretched hand',
                image: '/psr-games.png',
                route: '/game/rpsbot.html',
                bg: '/prs-game.png'
            },
            {
                id: 1,
                title: 'Tic Tac Toe',
                detail: 'Paper-and-pencil game for two players who take turns marking the spaces in a three-by-three grid with X or O',
                image: '/tictactoe-games.png',
                route: '/game/tictactoe',
                bg: '/tictactoe-gm.png'
            },
            {
                id: 2,
                title: 'Matching Image',
                detail: 'Require players to match similar elements. Participants need to find a match for a word, picture, or card',
                image: '/match-games.jpeg',
                route: '/game/matching-images',
                bg: '/memory-game-2.png'
            },
            {
                id: 3,
                title: 'Congklak',
                detail: 'Get as many shells as you can into your store house. Your store house is the hole at the end of the board on your left side',
                image: '/4.jpg',
                route: '/game',
                bg: '/congklak-bg.jpeg'
            },
            {
                id: 4,
                title: 'Go Game',
                detail: 'Abstract strategy board game for two players in which the aim is to surround more territory than the opponent',
                image: '/gogame.png',
                route: '/game',
                bg: '/gogame2.jpg'
            }
    ]

    const [gameData,setGameData]=useState(imgs[0])
    const handleClick=(index)=>{
        console.log(index)
        const gameSlider=imgs[index];
        setGameData(gameSlider)
    }

    function SampleNextArrow({onClick}) {
        return (
        <div className='arrow arrow-right' onClick={onClick}>
            <BsArrowRight/>
        </div>
        );
    }

    function SamplePrevArrow({onClick}) {
        return (
        <div className='arrow arrow-left' onClick={onClick}>
            <BsArrowLeft/>
        </div>
        );
    }

    function EmptyArrow({onClick}) {
        return (
          <div></div>
        );
    }

    const [slideIndex, setSlideIndex] = useState(0);

    const settings = {
        dots: true,
        infinite: true,
        // speed: 900,
        slidesToShow: 5,
        slidesToScroll: 1,
        // autoplay: true,
        // autoplaySpeed: 8000,
        beforeChange: (current, next)=>setSlideIndex(next),
        centerMode: true,
        nextArrow: <SampleNextArrow />,
        prevArrow: <SamplePrevArrow />,
        appendDots: (dots) => (
        <div>
            <ul style={{ margin: "5px" }}> {dots} </ul>
        </div>
        ),
        customPaging: (current, next) => (
        <div className={current === slideIndex ? 'dot dot-active' : 'dot'}>
        </div>
        ),
        responsive: [
        {
            breakpoint: 768,
            settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
            dots: false,
            nextArrow: <EmptyArrow />,
            prevArrow: <EmptyArrow />,
            }
        }]
    };

    return (
        <div className="main-games" id='games-pg'
        style={{ backgroundImage: `url(${gameData.bg})`}}
        >
            <div className='back-btn'>
                <Link href='/'>
                    <img src='/back-btn.png' alt=''></img>
                </Link>
                <Link href='/'>
                    <h4>Back to Homepage</h4>
                </Link>
            </div>
            <div className='title-game'>
                <h1>{gameData.title}</h1>
                <p>{gameData.detail}</p>
                <Link href={gameData.route}>
                    <button>Play Now</button>
                </Link>
            </div>
            <Slider className='games-slide' {...settings}>
                {imgs.map((data,i)=>
                <div className={i === slideIndex ? 'slide slide-active': 'slide'} key={i} style="width:200px;display:inline-block" >
                    <img className={gameData.id==i?"clicked":""} src={data.image} onClick={()=>handleClick(i)} height="70px" width="100px" />
                </div>
                )}
            </Slider>
        </div>
    );
}

export default Games;