import React, {useState, useEffect} from 'react'
import io from 'socket.io-client'
import Image from 'next/image'
import Cookies from 'js-cookie';
import { useCookies } from "react-cookie";
import jwt from "jwt-decode";


const Rock_paper_scrissors = () => {
    // let dataUser = Cookies.get("user-data");

    //   function getCookie(name) {
    //     var nameEQ = name + "=";
    //     var ca = document.cookie.split(";");
    //     for (var i = 0; i < ca.length; i++) {
    //       var c = ca[i];
    //       while (c.charAt(0) == " ") c = c.substring(1, c.length);
    //       if (c.indexOf(nameEQ) == 0)
    //         return c.substring(nameEQ.length, c.length);
    //     }
    //     return null;
    //   }
      // JSON.parse(getCookie("user-data"))[0].user

      const [createRoomBoxDisplay, setCreateRoomBoxDisplay] = useState("none");
      const [roomIdInputValue, setRoomIdInputValue] = useState("");
      const [gameplayChoicesDisplay, setGameplayChoicesDisplay] = useState("block");
      const [mainGPDisplay, setMainGPDisplay] = useState("flex"); //main gameplay display
      const [startScreenDisplay, setStartScreenDisplay] = useState("block");
      const [joinBoxRoomDisplay, setJoinBoxRoomDisplay] = useState("none");
      const [joinRoomInputValue, setJoinRoomInputValue] = useState("");
      const [errorMessageDisplay, setErrorMessageDisplay] = useState("none");
      const [errorMessageValue, setErrorMessageValue] = useState("");
      // const [waitMessageValue, setWaitMessageValue] = useState("");
      const [rockBG, setRockBG] = useState("transparent");
      const [paperBG, setPaperBG] = useState("transparent");
      const [scissorBG, setScissorBG] = useState("transparent");
      const [rock2BG, setRock2BG] = useState("transparent");
      const [paper2BG, setPaper2BG] = useState("transparent");
      const [scissor2BG, setScissor2BG] = useState("transparent");
      const [myScoreValue, setMyScoreValue] = useState(0);
      const [enemyScoreValue, setEnemyScoreValue] = useState(0);
      const [enemyNameValue, setEnemyNameValue] = useState("Waiting...");
      const [playerOneBG, setPlayerOneBG] = useState("transparent");
      const [playerTwoBG, setPlayerTwoBG] = useState("transparent");
      const [VStextDisplay, setVStextDisplay] = useState("flex");
      const [P1WinDisplay, setP1WinDisplay] = useState("none");
      const [ComWinDisplay, setComWinDisplay] = useState("none");
      const [DrawDisplay, setDrawDisplay] = useState("none");
      const [canChooseVar, setCanChooseVar] = useState(false);
      const [playerOneConnectedStat, setPlayerOneConnectedStat] = useState(false);
      const [playerTwoIsConnectedStat, setPlayerTwoIsConnectedStat] = useState(false);
      const [playerId, setplayerId] = useState(0);
      const [myChoice, setMyChoice] = useState("");
      // const [enemyChoice, setEnemyChoice] = useState("");
      const [roomId, setRoomId] = useState("");
      const [myScorePoints, setMyScorePoints] = useState("");
      const [enemyScorePoints, setEnemyScorePoints] = useState("");
      const [userVSRoom, setUserVSRoom] = useState([]);
      const [BGK, setBGK] = useState("");
      const [leaveRoom, setLeaveRoom] = useState(1);
      const [Username, setUsername] = useState("");

      

      const socket = io({transports:['websocket']});

      const [cookies,setCookies] = useCookies()

      const getHistoryByUserId = async () => {
        const token = cookies.tgtoken;
        const decodeToken = jwt(token);
        const loged_in_username = decodeToken.username;
        console.log(loged_in_username)

        if (loged_in_username == "") {
        alert("You Haven't logged in yet...");
        window.location.href = "/login";
        } else {
          setUsername(loged_in_username)
        }
      };
      useEffect(() => {
        getHistoryByUserId()
      }, []);

      //  Game variables
      let enemyChoice = "";
      // let roomId = "";
      // let myScorePoints = 0;
      // let enemyScorePoints = 0;
      // let userVSRoom = [];
      // let BGK = "";
      // let leaveRoom = 1;

      // btn to show create room
       function openCreateRoomBox1() {
        setGameplayChoicesDisplay("none");
        setCreateRoomBoxDisplay("block");
        console.log("klik 1")
      };
      // btn to cancel SHOW create room
       function cancelCreateActionBtn1() {
        setGameplayChoicesDisplay("block");
        setCreateRoomBoxDisplay("none");
        setErrorMessageDisplay("none");
      };
      // btn to create room dengan input
      function createRoomBtn1() {
        let id = [
          roomIdInputValue,
          Username,
        ];
        setErrorMessageValue("none");
        socket.emit("create-room", id);
        console.log("createroomButton udah di klik dengan id ", roomIdInputValue)

      };
      // btn to show create JOIN room
       function openJoinRoomBox1() {
        setGameplayChoicesDisplay("none");
        setJoinBoxRoomDisplay("block");
      };
      function openVsComRoomBox1(){
        window.location.href="/game/rock-paper-scissors/vsbot"
      }
      // btn to cancel show create JOIN room
       function cancelJoinActionBtn1() {
        setGameplayChoicesDisplay("block");
        setJoinBoxRoomDisplay("none");
        setErrorMessageValue("none");
      };

      // btn to JOIN ROOM
       function joinRoomBtn1() {
        let id = [
          joinRoomInputValue,
          Username
          // JSON.parse(Cookies.get("user-data"))[0].user,
        ]; // ambil data dari input join room
        setErrorMessageValue("none"); // set error message gone
        socket.emit("join-room", id); // send data input join room ke server
      };
      // btn to JOIN RANDOM ROOM
       function joinRandomBtn1() {
        // const username = "user2"
        // JSON.parse(Cookies.get("user-data"))[0].user;
        setErrorMessageValue("none"); // set error message gone
        socket.emit("join-random", Username); // send data join random room ke server
        console.log("Join Room Button telah di klik")
      };
      // btn when CLICK ROCK
       function rock1() {
        if (
          canChooseVar &&
          myChoice === "" &&
          playerOneConnectedStat &&
          playerTwoIsConnectedStat
        ) {
          setMyChoice( myChoice ="rock");
          choose(myChoice);
          socket.emit("make-move", { playerId, myChoice, roomId });
          console.log("Batu P1 telah di klik", playerId ," ", myChoice," ", roomId)
        }
      };
      // btn when CLICK PAPER
       function paper1() {
        if (
          canChooseVar &&
          myChoice === "" &&
          playerOneConnectedStat &&
          playerTwoIsConnectedStat
        ) {
          setMyChoice(myChoice="paper");
          choose(myChoice);
          socket.emit("make-move", { playerId, myChoice, roomId });
        }
      };
      // btn when CLICK GUNTING
       function scissor1() {
        if (
          canChooseVar &&
          myChoice === "" &&
          playerOneConnectedStat &&
          playerTwoIsConnectedStat
        ) {
          setMyChoice(myChoice="scissor");
          choose(myChoice);
          socket.emit("make-move", { playerId, myChoice, roomId });
        }
      };

      

      // // Socket
      // // dijalankan bila user P1 mginput userId yg sudah ada/room join tidak ada/penuh
      // socket.on("display-error", (error) => {
      //   setErrorMessageValue("block");
      //   setErrorMessageValue(error)
      //   console.log("SO :","display-error, L1")
      // });

      // //saat room telah terbentuk di server maka di feedback ke client SBB :
      // socket.on("room-created", (id) => {
      //   setplayerId(1);
      //   setRoomId(id);
      //   console.log("room id yang sudah di buat : ", id)
      //   setLeaveRoom(2);
      //   setStartScreenDisplay("none"); // menset start screen tak terlihat
      //   setMainGPDisplay("flex"); // menset game screen terlihat
      //   // setWaitMessage(true);
      //   console.log("SO :","room-created, L2")
      //   console.log("RoomID :",roomId)
      // });

      // // saat semua sudah join/P2 join
      // socket.on("room-joined", (id) => {
      //   setplayerId(2);
      //   setRoomId(id);
      //   setLeaveRoom(2);
      //   setPlayerOneConnectedStat(true);
      //   playerJoinTheGame(1); //merubah titik merah P1 menjadi hijau di screen P2
      //   // setWaitMessage(false); // menghapus wait message
      //   setStartScreenDisplay("none"); //menghapus display start screen
      //   setMainGPDisplay("flex"); //munculkan display start screen
      //   console.log("SO :","room-joined, L3")
      // });

      // // bila berhasil create maka di jalankan :
      // socket.on("player-1-connected", (userRoom) => {
      //   console.log(userRoom[0], userRoom[1]);
      //   playerJoinTheGame(1); // saat berhasil create, run funct ini untuk merubah titik merah menjadi hijau
      //   setPlayerOneConnectedStat(true); // menyimpan sementara bahwa P1 terkoneksi
      //   console.log("SO :","player-1-connected, L4")
      // });

      // //bila berhasil join maka :
      // socket.on("player-2-connected", (userRoom) => {
      //   console.log(userRoom[0], userRoom[1]);
      //   playerJoinTheGame(2); // saat berhasil create, run funct ini untuk merubah titik merah menjadi hijau
      //   setPlayerTwoIsConnectedStat(true); // menyimpan sementara bahwa P2 terkoneksi
      //   setCanChooseVar(true); // pilihan menjadi aktif
      //   // setWaitMessage(false); // menghapus wait message
      //   setUserVSRoom(userRoom);
      //   if (playerId === 1) {
      //     setEnemyNameValue(userRoom[1]);
      //   } else if (playerId === 2) {
      //     setEnemyNameValue(userRoom[0]);
      //   } else {
      //     setEnemyNameValue("Waiting...");
      //   }
      //   console.log("SO :","player-2-connected, L5")
      // });

      // //saat player 1 disconect maka reset
      // socket.on("player-1-disconnected", () => {
      //   reset();
      //   console.log("SO :","player-1-disconnected, L6")
      // });

      // //saat player 2 disconect maka :
      // socket.on("player-2-disconnected", () => {myChoice
      //   setCanChooseVar(false);
      //   playerTwoLeftTheGame();
      //   // setWaitMessage(true);
      //   setEnemyScorePoints(0);
      //   setMyScorePoints(0);
      //   displayScore();
      //   console.log("SO :","player-2-disconnected, L7")
      // });

      // socket.on("draw", (message) => {
      //   enemyChoose(enemyChoice);
      //   setVStextDisplay("none");
      //   setDrawDisplay("flex");
      //   saveToDataBase(0, 1, 0, enemyChoice, enemyChoice);
      //   console.log("SO :","draw, L8")
      // });

      // socket.on("player-1-wins", ({ myChoice, enemyChoice }) => {
      //   setVStextDisplay("none");
      //   if (playerId === 1) {
      //     enemyChoose(myChoice);
      //     saveToDataBase(1, 0, 0, enemyChoice, myChoice);
      //     setP1WinDisplay("flex");
      //     setMyScorePoints(myScorePoints++);
      //   } else {
      //     enemyChoose(enemyChoice);
      //     saveToDataBase(0, 0, 1, myChoice, enemyChoice);
      //     setComWinDisplay("flex");
      //     setEnemyScorePoints(enemyScorePoints++);
      //   }

      //   displayScore();
      //   console.log("SO :","player-1-wins, L9")
      // });

      // socket.on("player-2-wins", ({ myChoice, enemyChoice }) => {
      //   setVStextDisplay("none");
      //   if (playerId === 2) {
      //     enemyChoose(enemyChoice);
      //     saveToDataBase(1, 0, 0, myChoice, enemyChoice);
      //     setP1WinDisplay("flex");
      //     setMyScorePoints(myScorePoints++);
      //   } else {
      //     enemyChoose(myChoice);
      //     saveToDataBase(0, 0, 1, enemyChoice, myChoice);
      //     setComWinDisplay("flex");
      //     setEnemyScorePoints(enemyScorePoints++);
      //   }

      //   displayScore();
      //   console.log("SO :","player-2-wins, L10")
      // });
      // useEffect(() => {
      // }, [])
      // Functions
      function playerJoinTheGame(playerId) {
        if (playerId === 1) {
          setPlayerOneBG("green")
        } else {
          setPlayerTwoBG("green");
        }
      }

      // function setWaitMessage(display) {
      //   if (display) {
      //     let p = "Invite Other Player with this RoomID : "+ roomId +"| Waiting for another player to join...";
      //     setWaitMessageValue(p);
      //   } else {
      //     setWaitMessageValue("");
      //   }
      // }

      function reset() {
        setCanChooseVar(false);
        setPlayerOneConnectedStat(false);
        setPlayerTwoIsConnectedStat(false);
        setStartScreenDisplay("block");
        setGameplayChoicesDisplay("block");
        setMainGPDisplay("none");
        setJoinBoxRoomDisplay("none");
        setCreateRoomBoxDisplay("none");
        setPlayerTwoBG("red");
        setPlayerOneBG("red");
        setMyScorePoints(0);
        setEnemyScorePoints(0);
        displayScore();
        // setWaitMessage(true);
        resetVS();
        removeEnemyChoice();
      }

      function playerTwoLeftTheGame() {
        setPlayerTwoIsConnectedStat(false);
        setPlayerTwoBG("red");
      }

      function displayScore() {
        setMyScoreValue(myScorePoints);
        setEnemyScoreValue(enemyScorePoints);
      }

      function choose(choice) {
        if (choice === "rock") {
          setRockBG("#C4C4C4");
        } else if (choice === "paper") {
          setPaperBG("#C4C4C4");
        } else {
          setScissorBG("#C4C4C4");
        }
        setCanChooseVar(false);
      }

      function removeChoice(choice) {
        if (choice === "rock") {
          setRockBG("transparent");
        } else if (choice === "paper") {
          setPaperBG("transparent");
        } else {
          setScissorBG("transparent");
        }
        setCanChooseVar(true);
        setMyChoice("");
      }

      function enemyChoose(choice) {
        if (choice === "rock") {
          setRock2BG("#C4C4C4");
        } else if (choice === "paper") {
          setPaper2BG("#C4C4C4");
        } else {
          setScissor2BG("#C4C4C4");
        }
        // canChoose = false;
      }

      function iconChoose(enemyChoice) {
        if (enemyChoice === "rock") {
          setBGK("✊");
        } else if (enemyChoice === "paper") {
          setBGK("✋");
        } else {
          setBGK("✌");
        }
        return BGK;
      }
      function removeEnemyChoice() {
        setRock2BG("transparent");
        setPaper2BG("transparent");
        setScissor2BG("transparent");
      }
      function resetVS() {
        setVStextDisplay("flex");
        setP1WinDisplay("none");
        setComWinDisplay("none");
        setDrawDisplay("none");
      }

      function saveToDataBase(win, draw, lose, choice1, choice2) {
        let timestamp = Date.now();
        // let userCookie = Cookies.get("user-data");
        // let user = "user1"
        // JSON.parse(Cookies.get("user-data"))[0].user;
        let oponent = "";
        let WLD = win == 1 ? "(WIN)" : draw == 1 ? "(DRAW)" : "(LOSE)";
        let scheme = `YOU ${iconChoose(choice1)} 
          vs ENEMY ${iconChoose(choice2)} ${WLD}`;
        if (userVSRoom[1] === Username) {
          oponent = userVSRoom[0];
        } else if (userVSRoom[0] === Username) {
          oponent = userVSRoom[1];
        } else {
          oponent = "unknown";
        }
        // fetch(`/games/${JSON.parse(userCookie)[0]._id}`, {
        //   method: "post",
        //   headers: { "Content-type": "application/json" },
        //   body: JSON.stringify({
        //     user: user,
        //     win: win,
        //     draw: draw,
        //     lose: lose,
        //     scheme: scheme,
        //     oponent: oponent,
        //     timestamp: timestamp,
        //   }),
        // })
        //   .then((response) => {
        //     return response.json();
        //   })
        //   .then((result) => {
        //     console.log(scheme);
        //   })
        //   .catch((error) => console.log(error));

        //reset player choice
        setTimeout(() => {
          removeChoice(myChoice);
          removeEnemyChoice();
          resetVS();
        }, 2500);
      }
      //  function GPBackLink1() {
      //   if (mainGPDisplay === "flex" && leaveRoom === 2) {
      //     if (confirm("Are you  sure to leave this game room?")) {
      //       window.location.reload();
      //     }
      //   } else {
      //     window.location.href = "/";
      //   }
      // };



  return (
    <div>
        <div className="mainingameroom" >
            {/*  */}
      {/* START SCREEN CODE START */}{/* START SCREEN CODE START */}{/* START SCREEN CODE START */}
      <div className="startScreen" style={{display:startScreenDisplay}}>
        <div className="gameplayChoices" id="gameplayChoices" style={{display:gameplayChoicesDisplay}}>
          <button onClick={openCreateRoomBox1} id="openCreateRoomBox"  className="btn btn-lg mt-2 col-4">Create Room</button>
          <button onClick={openJoinRoomBox1} id="openJoinRoomBox"  className="btn btn-lg mt-2 col-4">Join Room</button>
          <a href="/games" style={{textDecoration: "none"}}>
            <button onClick={openVsComRoomBox1} id="openVsComRoomBox"  className="btn btn-lg mt-2 col-4">vs Computer</button>
          </a>
        </div>

        {/* create room */}
        <div className="createRoomBox mt-2" id="createRoomBox" style={{display:createRoomBoxDisplay}}>
          <div className="row">
            <input
              type="text"
              id="roomIdInput"
              className="form-control"
              style={{border: "none"}}
              onChange={(e) => {
                setRoomIdInputValue(e.target.value);
              }}
            />
            <button onClick={createRoomBtn1} id="createRoomBtn" className="btnCreateJoin">Create</button>
          </div>

          <button onClick={cancelCreateActionBtn1} id="cancelCreateActionBtn" className="cancel-action btn">
            Cancel
          </button>
        </div>
        {/* create room */}

        {/* Join Room */}
        <div className="joinBoxRoom mt-2" id="joinBoxRoom" style={{display:joinBoxRoomDisplay}}>
          <div className="join-with-id">
            <div className="row">
              <input
                type="text"
                id="joinRoomInput"
                className="form-control"
                style={{border: "none"}}
                onChange={(e)=>setJoinRoomInputValue(e.target.value)}
              />
              <button onClick={joinRoomBtn1} id="joinRoomBtn" className="btnCreateJoin">Join</button>
            </div>
            <button onClick={cancelJoinActionBtn1} id="cancelJoinActionBtn" className="cancel-action btn">
              Cancel
            </button>
          </div>

          <button onClick={joinRandomBtn1} className="joinRandomBtn btn" id="joinRandomBtn">
            Join Random
          </button>
        </div>
        {/* JoinRoom */}

        <div className="errorMessage" id="errorMessage" style={{display:errorMessageDisplay}}><p>{errorMessageValue}</p></div>
      </div>
      {/* START SCREEN CODE END */}{/* START SCREEN CODE END */}{/* START SCREEN CODE END */}

      <div id="mainGP" style={{display:mainGPDisplay}}>
        <div className="MainContent">
          <div className="Player1">
            {/* player 1 */}
            <span className="player">
              <span className="dot" id="playerOne" style={{background:playerOneBG}}></span> YOU :
              <span id="myScore">{myScoreValue}</span>
            </span>
            <button onClick={rock1} id="rock" style={{backgroundColor:rockBG}} className="optionBtnP1">
              <Image src="/img/batu.png" alt="batu" height="90px" width="100px"/>
            </button>
            <button onClick={paper1} id="paper" style={{backgroundColor:paperBG}} className="optionBtnP1">
              <Image src="/img/kertas.png" alt="batu"height="120px" width="100px"/>
            </button>
            <button onClick={scissor1} id="scissor" style={{backgroundColor:scissorBG}} className="optionBtnP1">
              <Image src="/img/gunting.png" alt="batu"height="120px" width="100px"/>
            </button>
            <h6 id="playerName">{Username}</h6>
          </div>

          <div className="midcontainer">
            <div className="waitMessage" id="waitMessage"><p>Game ID : {roomId}</p></div>
            <h1 id="VStext" style={{display:VStextDisplay}}>VS</h1>
            <div id="P1Win" style={{display:P1WinDisplay}}>YOU WIN</div>
            <div id="ComWin" style={{display:ComWinDisplay}}>YOU LOSE</div>
            <div id="Draw" style={{display:DrawDisplay}}>DRAW</div>
          </div>
          <div className="Player1">
            {/* player2 */}
            <span className="player">
              <span className="dot" id="playerTwo" style={{background:playerTwoBG}}></span> ENEMY :
              <span id="enemyScore">{enemyScoreValue}</span>
            </span>
            <button id="rock2" style={{backgroundColor:rock2BG}} className="optionBtnCom">
              <Image src="/img/batu.png" alt="batu" className="optionP1" height="90px" width="100px"/>
            </button>
            <button id="paper2" style={{backgroundColor:paper2BG}} className="optionBtnCom">
              <Image src="/img/kertas.png" alt="kertas" className="optionP1" height="120px" width="100px"/>
            </button>
            <button id="scissor2"style={{backgroundColor:scissor2BG}} className="optionBtnCom">
              <Image src="/img/gunting.png" alt="gunting" className="optionP1" height="120px" width="100px"/>
            </button>
            <h6 id="enemyName" className="align-self-center">{enemyNameValue}</h6>
          </div>
        </div>
      </div>
    </div>

    {/* Header */}
    {/* <div className="gameTitle">
      <a className="GPBackLink" id="GPBackLink" onClick={GPBackLink1}>&lt;</a>
      <Image id="GPlogo" src="/img/gamePLogo.png" alt="logoLamanGame" layout='fill' />
      <h3 id="gamePjudul">ROCK PAPER SCRISSORS</h3>
    </div> */}

    </div>
  )
}

export default Rock_paper_scrissors