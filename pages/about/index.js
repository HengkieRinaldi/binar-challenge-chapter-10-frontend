import React, { useState } from 'react'
import NavBar from '../../components/NavBar'
import {
    Card,
    CardHeader,
    CardBody,
    Collapse
  } from "reactstrap";
import Link from 'next/link';
import Card3 from '../../components/Card/Card3'

const About = (idx) => {
    const [openedCollapse, setOpenedCollapse] = useState("collapseOne")

    return (
        <div id='about-us'>
            <NavBar />
            <div className='container' id='about-bg'>
                <div className='title-about text-center'>
                    <h1>ABOUT <span>US</span></h1>
                </div>
            </div>
            <div className='container' id='about-detail'>
                <div className='describe-gm text-center'>
                    <h1>We Make <span>Great</span> Games</h1>
                    <p>Our games are sprinkled with a whirlwind of smile-inducing awesomeness and polished to a shiny sheen that keeps the world coming back again and again. And best of all, you will find our games on mobile, console, PC, and many other devices. You may even be able to play them on the moon!</p>
                </div>
                <div className='collapse-card' id='accordian-about'>
                    <Card>
                        <CardHeader
                            id="headingOne"
                            aria-expanded={openedCollapse === "collapseOne"}
                            onClick={() =>
                                setOpenedCollapse( openedCollapse === "collapseOne" ? "" : "collapseOne" )}
                        >
                            <h5 className="mb-0">
                                How Do You Play?
                            </h5>
                        </CardHeader>
                        <Collapse
                            isOpen={openedCollapse === "collapseOne"}
                            aria-labelledby="headingOne"
                            data-parent="#accordian-about"
                            id="collapseOne"
                        >
                            <CardBody className=" opacity-8">
                                GAMX are available on a wide variety of digital platforms. You can purchase and download our games anywhere from an Xbox One to your Apple TV! Once you have bought one of our games, its yours to own and play as much as you want.
                                <br></br><br></br>
                                To play, each player needs a phone or other web-enabled device to use as their controller. When you start a game, you will be given a unique room code on your screen. Just pull up Jackbox.tv on your devices web browser, and enter the room code to play along.
                                <br></br><br></br>
                                Our games are for anywhere from 1-2 players. And, if you have more people wanting to play along, they can join as an audience member to influence the games outcome.
                            </CardBody>
                        </Collapse>
                    </Card>
                    <Card>
                        <CardHeader
                            id="headingTwo"
                            aria-expanded={openedCollapse === "collapseTwo"}
                            onClick={() =>
                                setOpenedCollapse( openedCollapse === "collapseTwo" ? "" : "collapseTwo" )}
                        >
                            <h5 className=" mb-0">
                                What is a Party Pack?
                            </h5>
                        </CardHeader>
                        <Collapse
                            isOpen={openedCollapse === "collapseTwo"}
                            aria-labelledby="headingTwo"
                            data-parent="#accordian-about"
                            id="collapseTwo"
                        >
                            <CardBody className=" opacity-8">
                                We have been releasing collections of easy-to-play party games for your friends, family, and fellow inmates.
                                <br></br>
                                Players join by simply using the web browser on their smartphone, no app needed! 
                                <br></br><br></br>
                                Available on just about every platform except the smart fridge, the Jackbox Party Packs are ready to take your get-together to the next level. Or at least to the level right below that one.
                            </CardBody>
                        </Collapse>
                    </Card>
                    <Card>
                        <CardHeader
                            id="headingTwo"
                            aria-expanded={openedCollapse === "collapseThree"}
                            onClick={() =>
                                setOpenedCollapse( openedCollapse === "collapseThree" ? "" : "collapseThree" )}
                        >
                            <h5 className=" mb-0">
                                How Can I Get In Touch?
                            </h5>
                        </CardHeader>
                        <Collapse
                            isOpen={openedCollapse === "collapseThree"}
                            aria-labelledby="headingThree"
                            data-parent="#accordian-about"
                            id="collapseThree"
                        >
                            <CardBody className=" opacity-8">
                                For other information and questions, visit our Contact page <Link href={'/contact'}>Here</Link>
                            </CardBody>
                        </Collapse>
                    </Card>
                </div>
            </div>
            <div className='row card-three text-center' id='our-team'>
                <h1>OUR GREAT <span>TEAMS</span></h1>
                <div className='card-three'>
                    <Card3 />
                </div>
            </div>
        </div>
  )
}

export default About;