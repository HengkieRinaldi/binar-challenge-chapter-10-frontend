// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getAnalytics } from "firebase/analytics";
import { getAuth } from "firebase/auth";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyC96Gulkv1Et62_A1HRvszdxQGNVFJhYh0",
  authDomain: "traditional-game-react-app.firebaseapp.com",
  projectId: "traditional-game-react-app",
  storageBucket: "traditional-game-react-app.appspot.com",
  messagingSenderId: "152912824991",
  appId: "1:152912824991:web:b6cde54907893627b3b0aa",
  measurementId: "G-MMWGK6YXQ8",
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);

export const auth = getAuth(app);
export default app;
