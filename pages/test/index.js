import { useState } from "react";
import jwt from "jwt-decode"
import {useCookies} from "react-cookie"
import { Button } from "react-bootstrap";

export default function Test() {
    const [cookies,setCookies] = useCookies()
    const testing = (e) => {
        const token = cookies.token
        const decodeToken = jwt(token)
        const id = decodeToken.id
        const username = decodeToken.username
        const type = decodeToken.type
        console.log(`ini tokennya : ${token}`)
        console.log("ini id dari token :" + id)
        console.log("ini username dari token :" + username)
        console.log("ini type user dari token :" + type)
    }
    
    return (
        <>
        <Button onClick={testing}>test token</Button>
        </>
    )
}