import React, { useState, useEffect } from 'react'
import { Navbar, Container, Nav, NavLink } from "react-bootstrap";
import { useCookies } from "react-cookie";
import jwt from "jwt-decode";

const NavBar = () => {
  const [cookies, setCookies, deleteCookies] = useCookies()

  const [signUpBtnDisplay, setSignUpBtnDisplay] = useState("flex")
  const [logInBtnDisplay, setLogInBtnDisplay] = useState("block")
  const [profileBtnDisplay, setProfileBtnDisplay] = useState("flex")
  const [Username, setUsername] = useState("username")
  const [LogInOut, setLogInOut] = useState("Login")
  const [LogInOutStateState, setLogInOutStateState] = useState("blm_login")
  const [ProfileImage, setProfileImage] = useState("blankpp.webp")
  // const [usernameTxt, setUsernameTxt] = useState("flex")

  // const [cookies] = useCookies(["tgtoken"]);
  // const token = cookies.tgtoken;
  // if(token){
  //   const decodeToken = jwt(token);
  //   const username = decodeToken.username;
  //   setUsernameTxt(user)
  // }
  // console.log("Ini Cookies", cookies)

  const getUsernameByToken = async () => {
    const token = cookies.tgtoken;
    const googleLogin = cookies.googlelogin;
    if (token) {
      const decodeToken = jwt(token);
      const UsernameToken = decodeToken.username;
      setUsername(UsernameToken);
      setProfileBtnDisplay("flex")
      setSignUpBtnDisplay("none")
      setLogInOutStateState("udh_login")
      setLogInBtnDisplay("none")
      setLogInOut("Logout")
    } else if(googleLogin){
      const glogin = googleLogin
      const googleProfileImage = await glogin.providerData[0].photoURL;
      if(googleProfileImage){
        setProfileImage(glogin.providerData[0].photoURL)
      }else{
        setProfileImage("blankpp.webp")
      }    
      setUsername(glogin.email);
      setProfileBtnDisplay("flex")
      setSignUpBtnDisplay("none")
      setLogInOutStateState("udh_login")
      setLogInOut("Logout")
      setLogInBtnDisplay("none")
    } else {
      setProfileBtnDisplay("none")
      setSignUpBtnDisplay("flex")
      setLogInOutStateState("blm_login")
      setLogInOut("Login")
      setLogInBtnDisplay("flex")
    }
  };

  useEffect(() => {
    getUsernameByToken()
  }, []);

  function loginout() {
    if (LogInOutStateState === "blm_login") {
      window.location.href = "/login"
    } else {
      deleteCookies("tgtoken")
      deleteCookies("googlelogin")
      window.location.href = "/"
    }

  }
  return (
    <Navbar collapseOnSelect expand="lg" variant="dark" id="navigation">
      <Container fluid>
        <Navbar.Brand href="/">
          <img src='gaming-logo.png' alt='' />
        </Navbar.Brand>
        <Navbar.Toggle aria-controls="responsive-navbar-nav" />
        <Navbar.Collapse id="responsive-navbar-nav">
          <Nav className="nav-left">
            <Nav.Link href="/">Home</Nav.Link>
            <Nav.Link href="/game">Games</Nav.Link>
            <Nav.Link href="/about">About Us</Nav.Link>
            <Nav.Link href="/blog">Blog</Nav.Link>
            <Nav.Link href="/contact">Contact</Nav.Link>
          </Nav>
          <Nav className="nav-right">
            <Nav.Link href="/user-profile" style={{ display: profileBtnDisplay, alignItems: "center", marginRight:"2px" }}>
              {Username}<img src={ProfileImage} alt="blankpp.webp" className="avatar" /></Nav.Link>
            <Nav.Link href="/signup" style={{ display: signUpBtnDisplay }}><button>Sign Up</button></Nav.Link>
            <Nav.Link style={{ display: logInBtnDisplay}} 
            onClick={loginout}><button>{LogInOut}</button></Nav.Link>

          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  )
}

export default NavBar