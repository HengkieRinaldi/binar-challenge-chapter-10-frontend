import SideBar from "./SideBar"
import Head from "next/head"
import { useState, useEffect } from "react";
import axios from "axios";
import jwt from "jwt-decode";
import { useCookies } from "react-cookie";
import { Table } from "react-bootstrap";

const GetUser = () => {
    const [cookies,setCookies] = useCookies()
    const [userHistory, setUserHistory] = useState([]);
        // const curentport = "http://localhost:7070"
        const curentport = "https://traditionalgame.herokuapp.com"

    const getHistoryByUserId = async () => {
        const token = cookies.tgtoken;
        const token2 = cookies.googlelogin;
        let user_id ="";
        if(token){
            const decodeToken = jwt(token);
            user_id = decodeToken.id
        }else{
            user_id = token2.uid;
        }
        const response = await axios.get(
            `${curentport}/api/history/${user_id}`,
            {
                withCredentials: false,
            }
        );
        console.log("History message :" +response.data.message)
        if(response.data.result){
            console.log(response.data.result);
            setUserHistory(response.data.result);
            for (let i = 0; i < response.data.result.length; i++) {
                console.log("test", [i]);
            }
        }
    };


    useEffect(() => {
        getHistoryByUserId()
    }, []);

    return (
        <>
            <Head>
                <title>GAMX | PROFILE</title>
            </Head>
            <div className='container' id='user-profile'>
                <div className='row'>
                    <div className='sidebar-profile col-md-2'>
                        <SideBar />
                    </div>
                    <div className="container col-md-9" id="history">
                        <h1>History Games</h1>
                        <p className="p-col-md-6">If you log in to the same account to play games on more than one device, you can see all the games you have played.</p>
                        <Table>
                            <thead>
                                <tr className="justify-content-center text-center">
                                    <th>User</th>
                                    <th>Win</th>
                                    <th className="justify-content-center text-center">Draw</th>
                                    <th className="justify-content-center text-center">Lose</th>
                                    <th className="justify-content-center text-center">Scheme</th>
                                    <th className="justify-content-center text-center">Openent</th>
                                    <th className="justify-content-center text-center">Time Stamp</th>
                                </tr>
                            </thead>
                            <tbody>
                                {userHistory.map((a, b) => (
                                    <tr key={b}>
                                        <td>{a.username}</td>
                                        <td>{a.win}</td>
                                        <td>{a.draw}</td>
                                        <td>{a.lose}</td>
                                        <td>{a.scheme}</td>
                                        <td>{a.oponent}</td>
                                        <td>{a.timestamp}</td>
                                    </tr>
                                ))}
                            </tbody>
                        </Table>
                    </div>
                </div>
            </div>
        </>
    )
}

export default GetUser