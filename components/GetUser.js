import SideBar from "./SideBar"
import Head from "next/head"
import Link from 'next/link'
import { useState, useEffect } from "react";
import axios from "axios";
import jwt from "jwt-decode";
import { useCookies } from "react-cookie";

const GetUser = () => {
    const [cookies, setCookies] = useCookies();
    const [first_name, setFirst_name] = useState("");
    const [photoURL, setphotoURL] = useState("/icons8-avatar-64.png");
    const [username, setUsername] = useState("");
    const [email, setEmail] = useState("");
    const [phone, setPhone] = useState("");
    const [birth, setBirth] = useState("");
    const [gender, setGender] = useState("");
    const [address, setAddress] = useState("");

    const getUserById = async () => {
        const token = cookies.tgtoken;
        if(token){
            const decodeToken = jwt(token);
            const id = decodeToken.id;
            // const curentport = "http://localhost:7070"
            const curentport = ""
        
            const response = await axios.get(`${curentport}/api/user/${id}`, {
              withCredentials: false,
            });
            
            console.log(response.data);
            setFirst_name(response.data.first_name);
            setUsername(response.data.username);
            setEmail(response.data.email);
            setPhone(response.data.phone);
            setBirth(response.data.birth);
            setGender(response.data.gender);
            setAddress(response.data.address);
        }else{
            const gdata = cookies.googlelogin;
            setphotoURL(gdata.providerData[0].photoURL)
            setUsername(gdata.displayName)
            setEmail(gdata.email);
            setPhone("no Number");
            setBirth("undifined");
            setGender("undifined");
            setAddress("undifined");
        }
      };

      useEffect(() => {
        getUserById();
      }, []);

    return (
        <>
            <Head>
                <title>GAMX | PROFILE</title>
            </Head>
            <div className='container' id='user-profile'>
                <div className="row">
                    <div className="sidebar-profile col-md-2">
                        <SideBar />
                    </div>
                    <div className="container col-md-9" id="overview">
                        <div className="col-md-6">
                            <h1>Overview</h1>
                            <p className="p-col-md-6">Welcome back, name! Lets play the games</p>
                            <div className="box-img">
                                <img src="/gamebox.png" alt=""></img>
                                <img className="rps-img" src="/rockpaperstrategy-1600.jpg" alt=""></img>
                            </div>
                            <div className="featured-games">
                                <h4>Featured Games</h4>
                                <p>Games List</p>
                                <ul>
                                    <Link href='/'><li>Tic tac toe</li></Link>
                                    <Link href='/'><li>Matching images</li></Link>
                                    <Link href='/'><li>Rock paper scissors</li></Link>
                                </ul>
                            </div>
                        </div>
                        <div className="col-md-2">
                            <div className="prf">
                                <div className="sm-prf">
                                    <img src={photoURL} alt=""></img>
                                    <h4>{first_name}</h4>
                                    <p>Member</p>
                                </div>
                                <hr></hr>
                                <div className="column details-prf">
                                    <p>Username</p>
                                    <span>{username}</span>
                                    <p>Email</p>
                                    <span>{email}</span>
                                    <p>Phone</p>
                                    <span>{phone}</span>
                                    <p>Date of Birth</p>
                                    <span>{birth}</span>
                                    <p>Gender</p>
                                    <span>{gender}</span>
                                    <p>Address</p>
                                    <span>{address}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}

export default GetUser