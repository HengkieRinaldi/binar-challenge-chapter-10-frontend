import React from 'react'
import { useState } from 'react'
import Slider from 'react-slick'
import {BsArrowLeft, BsArrowRight,} from 'react-icons/bs';
import Image from 'next/image'
import Link from 'next/link';

const SliderImg = [
  {
    id: 'match',
    image: '/match-games.jpeg',
    url: '/game/matching-images'
  },
  {
    id: 'congklak',
    image: '/congklak-2.png',
    url: '/game'
  },
  {
    id: 'tictactoe',
    image: '/tictactoe-games.png',
    url: '/game/tictactoe'
  },
  {
    id: 'psr',
    image: '/psr-games.png',
    url: '/game/rock-paper-scrissors'
  },
  {
    id: 'gogame',
    image: '/gogame.png',
    url: '/game'
  }
]

function SampleNextArrow({onClick}) {
    return (
      <div className='arrow arrow-right' onClick={onClick}>
        <BsArrowRight/>
      </div>
    );
  }

function SamplePrevArrow({onClick}) {
    return (
      <div className='arrow arrow-left' onClick={onClick}>
        <BsArrowLeft/>
      </div>
    );
  }

function EmptyArrow({onClick}) {
    return (
      <div></div>
    );
}

function Carousel1 () {


    const [slideIndex, setSlideIndex] = useState(0);

    const settings = {
        dots: true,
        infinite: true,
        speed: 1000,
        slidesToShow: 3,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 9000,
        beforeChange: (current, next)=>setSlideIndex(next),
        centerMode: true,
        nextArrow: <SampleNextArrow />,
        prevArrow: <SamplePrevArrow />,
        appendDots: (dots) => (
        <div>
            <ul style={{ margin: "5px" }}> {dots} </ul>
        </div>
        ),
        customPaging: (current, next) => (
        <div className={current === slideIndex ? 'dot dot-active' : 'dot'}>
        </div>
        ),
        responsive: [
        {
            breakpoint: 768,
            settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
            dots: false,
            nextArrow: <EmptyArrow />,
            prevArrow: <EmptyArrow />,
            }
        }]
    };

    return (
            <div className='carousel-one' id='carousel1'>
                <div className='slider'>
                    <Slider {...settings}>
                        {SliderImg.map((img, index) => (
                            <div className={index === slideIndex ? 'slide slide-active': 'slide'} key={index}>
                            <Link href={img.url}>
                              <Image src={img.image} alt="" layout='fill' />
                            </Link>
                        </div>
                        ))}
                    </Slider>
                </div>
            </div>
    )
}

export default Carousel1