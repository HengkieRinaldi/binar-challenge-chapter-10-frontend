import React, { useState, useEffect } from 'react'
import axios from 'axios'

const ImageHandling = () => {
    const [data, setData] = useState([]);
    // const curentport = "http://localhost:7070"
    const curentport = ""    
    
    const getData= async () =>{
    try {
        const res = await axios.get(`${curentport}/image`)
        setData(res.data)
    } catch (err) {
        console.log(err)
    }
   }
    useEffect(() => {
        getData()
    },[]);
    return (
        <div className="App">
            <h1>Image uploading react</h1>
            {data.map((singleData,keys) => {
                // convert binary to img
                const base64String = btoa(
                    String.fromCharCode(...new Uint8Array(singleData.image.data.data))
                );
                return (
                    <div key={keys}>
                    <h1> {singleData.title} </h1>
                    <img src={`data:image/png;base64,${base64String}`} width="300" />
                    </div>
                )
                
            })}
        </div>
    );
}
export default ImageHandling