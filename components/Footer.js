import React from 'react'
import Link from 'next/link'

const Footer = () => {
  return (
    <div className='container' id='footer'>
        <div className='text-center col-md-10'>
            <hr></hr>
            <div className='row'>
                <div className='text-left'>
                    <p className='follow'>Follow Us:</p>
                    <a href='https://www.instagram.com/danthemangame/?hl=id' target={"_blank"} rel={"noreferrer"}>
                        <img src='./instagram.svg' alt='' />
                    </a>
                    <a href='https://www.facebook.com/GameSpot' target={"_blank"} rel={"noreferrer"} >
                        <img src='./facebook.svg' alt='' />
                    </a>
                    <a href='https://twitter.com/buildintogames' target={"_blank"} rel={"noreferrer"} >
                        <img src='./twitter.svg' alt='' />
                    </a>
                </div>
                <div className='text-right'>
                    <img src='./heroicons-outline_phone.png' alt='' />
                    <p>+ (00) 123 456 789</p>
                    <img src='./mdi-light_email.png' alt='' />
                    <p>gamx-binar@gmail.com</p>
                </div>
            </div>
            <hr></hr>
            <div className='bottom'>
                <p>Copyright 2022 Vectcreation | All rights reserved.</p>
            </div>
        </div>
    </div>
  )
}

export default Footer