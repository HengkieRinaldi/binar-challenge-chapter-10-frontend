import React from 'react'
import Link from 'next/link'
import SidebarMenu from 'react-bootstrap-sidebar-menu';
import { useCookies } from "react-cookie";

const SideBar = () => {
    const [cookies, setCookies, deleteCookies] = useCookies()
    function logOutBtn(){
        // deleteCookies("tgtoken")
        // deleteCookies("googlelogin")
        // deleteCookies("user-data")
        deleteCookie("tgtoken")
        deleteCookie("googlelogin")
        deleteCookie("user-data")
        console.log("cookie di delete")      
        window.location.href = "/"
    }
    function deleteCookie(name) {
        document.cookie = name + "=;" + `expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;`;
    }

  return (
      <SidebarMenu>
          <SidebarMenu.Header>
              <SidebarMenu.Brand className='row logo-profile'>
                    <Link href='/'>
                        <img src='/gaming-logo.png' alt='' />
                    </Link>
                    <Link href='/'>
                        <h2>GAM<span>X</span></h2> 
                    </Link> 
              </SidebarMenu.Brand>
              {/* <SidebarMenu.Toggle className='responsive-sidebar-nav'/> */}
              <hr></hr><hr></hr><hr></hr>
          </SidebarMenu.Header>
          <SidebarMenu.Nav className='column menu-profile'>
            <SidebarMenu.Nav.Link className='row overview' href='/user-profile'>
                <img src='/icons8-person-32.png' alt='' />
                <h4>Overview</h4>
            </SidebarMenu.Nav.Link>
            <SidebarMenu.Nav.Link className='row history' href='/user-profile/history'>
                <img src='/icons8-history-32.png' alt='' />
                <h4>History</h4>
            </SidebarMenu.Nav.Link>
            <SidebarMenu.Nav.Link className='row summary' href='/user-profile/summary'>
                <img src='/icons8-overview-48.png' alt='' />
                <h4>Summary</h4>
            </SidebarMenu.Nav.Link>
            <SidebarMenu.Nav.Link className='row settings' href='/user-profile/editprofile'>
                <img src='/icons8-gear-32.png' alt='' />
                <h4>Settings</h4>
            </SidebarMenu.Nav.Link>
            <SidebarMenu.Nav.Link className='row log-out' onClick={logOutBtn}>
                <img src='/icons8-logout-48.png' alt='' />
                <h4>Log out</h4>
            </SidebarMenu.Nav.Link>
          </SidebarMenu.Nav>
      </SidebarMenu>
  )
}

export default SideBar;