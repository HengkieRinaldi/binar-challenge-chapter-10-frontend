import React from 'react'

const Abilities = [
    {
        name: 'box-card1',
        img: '/octicon_people-24.png',
        title: 'Experience Trainers',
        details: `In This Article, You'll Get to Know All The Necessary Training Skills.`
    },
    {
        name: 'box-card2',
        img: '/carbon_game-console.png',
        title: 'Every Console is Here',
        details: 'The Easiest Console to Find Right Now is Also The Most Popular.'
    },
    {
        name: 'box-card3',
        img: '/fluent_phone-laptop-24-regular.png',
        title: 'Streamer Supporting',
        details: 'Support is One of The Most Macro-Heavy and Decision-Intensive Roles in The Game.'
    }
]

const Card1 = (props) => {
  return Abilities.map((list, index)=> 
    <div className='wrap-card1' key={index}>
        <div className={list.name}>
            <img className='img-card1' src={list.img} alt='' />
            <h4 className='title-card1'>{list.title}</h4>
            <p className='details1'>{list.details}</p>
        </div>
    </div>
    )
}

export default Card1