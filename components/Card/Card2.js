import React from 'react'

const NumUser = [
    {   
        id: 1,
        title: '100+',
        details: 'Games launched'
    },
    {
        id: 2,
        title: '10B+',
        details: 'Downloads'
    },
    {
        id: 3,
        title: '200M',
        details: 'Monthly active users'
    },
    {
        id: 4,
        title: '10+',
        details: 'Languages'
    }
]

export const Card2 = () => {
  return NumUser.map((value, index) => 
    <div className='wrap-card2' key={index}>
        <div className='boxtwo'>
            <h4 className='title-card2'>{value.title}</h4>
            <p className='details2'>{value.details}</p>
        </div>
    </div>
  )
}
