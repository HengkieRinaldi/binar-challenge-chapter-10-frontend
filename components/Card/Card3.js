import React from 'react'

const Profile = [
    {
        photo: 'https://media-exp1.licdn.com/dms/image/C5603AQG967HIC06h5g/profile-displayphoto-shrink_800_800/0/1614301027618?e=2147483647&v=beta&t=-JLJk8ngJaxUojfeTz9SKP3-QAe_W5Kt1TUUrZE5ZQE',
        name: 'faiz Muttaqin',
        position: 'Scrum Master'
    },
    {
        photo: 'https://media-exp1.licdn.com/dms/image/C5603AQHkkuyXUziDuw/profile-displayphoto-shrink_800_800/0/1660156628610?e=1673481600&v=beta&t=1VyHlnzHnQFwzHNrvX7Jdd1LkCQdc747enJ8LhOwkkE',
        name: 'Ananda Maryam Pradini',
        position: 'UI/UX Concept Designer'
    },
    {
        photo: 'https://media-exp1.licdn.com/dms/image/C5603AQFttVhPcnwMMg/profile-displayphoto-shrink_400_400/0/1663748386150?e=1673481600&v=beta&t=O2UOZQi46idHYQUQ4N3ewPCUqP6DD1kQyv14Z7vd2q4',
        name: 'Hengkie Rinaldi Nugroho',
        position: 'Backend Systems Designer'
    },
    {
        photo: 'https://media-exp1.licdn.com/dms/image/D5635AQEDwacHETdHMA/profile-framedphoto-shrink_400_400/0/1666960884579?e=1668355200&v=beta&t=pTh0PQTMI2WTnSpVBqVqc23QpvfcYKC3y8mxAsbtXds',
        name: 'Ardiano Vito Saputra',
        position: 'Auth Security Designer'
    }

]

const Card3 = (props) => {
    return Profile.map((people, index)=> 
        <div className='wrap-card3' key={index}>
            <div className='card3'>
                <img className='img-card3' src={people.photo} alt='' />
                <h4 className='title-card3'>{people.name}</h4>
                <p className='details3'>{people.position}</p>
            </div>
        </div> 
    )
}

export default Card3