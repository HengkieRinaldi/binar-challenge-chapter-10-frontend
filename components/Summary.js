import SideBar from "./SideBar"
import Head from "next/head"
import { useState, useEffect } from "react";
import axios from "axios";
import jwt from "jwt-decode";
import { useCookies } from "react-cookie";
import { Table } from "react-bootstrap";

const Summary = () => {
    const [cookies, setCookies] = useCookies()
    const [username, setUsername] = useState("");
    const [sejarah, setSejarah] = useState([]);
    // const curentport = "http://localhost:7070"
    const curentport = "https://traditionalgame.herokuapp.com"
    let w = 0;
    let d = 0;
    let l = 0;

    if (sejarah) {
        for (let i = 0; i < sejarah.length; i++) {
            w += sejarah[i].win;
            d += sejarah[i].draw;
            l += sejarah[i].lose;
        }
    }

    const getUserById = async () => {
        const token = cookies.tgtoken;
        const token2 = cookies.googlelogin;
        let user="";
        if(token){
            const decodeToken = jwt(token);
            user = decodeToken.username
        }else{
            user = token2.email;
        }
        // const response = await axios.get(`${curentport}/api/user/${id}`, {
        //   withCredentials: false,
        // });
        
        setUsername(user);
      };

    const getSejarah = async () => {
        const token = cookies.tgtoken;
        const token2 = cookies.googlelogin;
        let user_id ="";
        if(token){
            const decodeToken = jwt(token);
            user_id = decodeToken.id
        }else{
            user_id = token2.uid;
        }
        const response = await axios.get(
            `${curentport}/api/history/${user_id}`,
            {
                withCredentials: false,
            }
        );
        if(response.data.result){
            console.log(response.data.result);
            setSejarah(response.data.result);
            for (let i = 0; i < response.data.result.length; i++) {
                console.log("test", [i]);
            }
        }
    };

    useEffect(() => {
        getSejarah()
        getUserById()
    }, []);

    return (
        <>
            <Head>
                <title>GAMX | PROFILE</title>
            </Head>
            <div className='container' id='user-profile'>
                <div className='row'>
                    <div className='sidebar-profile col-md-2'>
                        <SideBar />
                    </div>
                    <div className="container col-md-9" id="summary">
                        <h1>Your Summary</h1>
                        <p className="p-col-md-6">You win some, You lose some!</p>
                        <Table>
                            <thead>
                                <tr className="justify-content-center text-center">
                                    <th>User</th>
                                    <th>Win</th>
                                    <th className="justify-content-center text-center">Draw</th>
                                    <th className="justify-content-center text-center">Lose</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr className="justify-content-center text-center">
                                    <td>{username}</td>
                                    <td>{w}</td>
                                    <td>{d}</td>
                                    <td>{l}</td>
                                </tr>
                            </tbody>
                        </Table>
                    </div>
                </div>
            </div>
        </>
    )
}

export default Summary